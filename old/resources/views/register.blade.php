<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="icon" href="{{('assets/img/basic/favicon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <title>Vendor Register</title>
    <!-- CSS -->
    <link rel="stylesheet" href="{{('assets/css/app.css')}}">
      <link rel="stylesheet" href="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css')}}">
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }
    </style>
</head>
<body class="light">
<!-- Pre loader -->
 <div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div> 
<div id="app">
<main id="main" class="site-main">
            <div class="container">
                <div class="col-xl-8 mx-lg-auto p-t-b-80">
                    <header class="text-center">
                        <h1>Create New Account</h1>
                        <p>Join Our wonderful community and let others help you without a single
                            penny</p>
                        <img class="p-t-b-50" src="{{('assets/img/icon/icon-join.png') }}" alt="">
                    </header>
                    <form class="btn-submit" id="add_form" method="POST">
                      <!-- @csrf -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                  <select class="form-control " name="country_id">
                                      <option  selected disabled hidden value="select">Select Country</option>
                                      @foreach($countries_data as $value)
                                      <option value="{{$value->id}}">{{$value->name}}</option>
                                      @endforeach
                                      
                                    </select>
                                    <span class="text-danger"id="country_err" style="color:red;"></span>
                                  </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="name" class="form-control form-control-lg" placeholder="First Name">
                                    <span class="text-danger"id="name_err" style="color:red;"></span>
                                </div>
                            </div>
                            
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="last_name"  class="form-control form-control-lg" placeholder="Last Name">
                                    <span class="text-danger"id="last_name_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control form-control-lg" placeholder="Email Address">
                                    <span class="text-danger"id="email_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="phone" class="form-control form-control-lg" maxlength="10" minlength="10" placeholder="##########">
                                    <span class="text-danger"id="phone_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text"  name="company_name" class="form-control form-control-lg" placeholder="Company Name">
                                    <span class="text-danger"id="company_name_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group" id="i_am_a">
                                <lable class="col-lg-12">I am A:</lable><br><br>
                                  <input type="radio" id="supplier" name="i_am_a" value="supplier">
                                  <label for="Supplier">Supplier</label>
                                <input type="radio" id="buyer" name="i_am_a" value="buyer">
                                  <label for="buyer">Buyer</label>
                                  <input type="radio" id="both" name="i_am_a" value="both">
                                  <label for="both">Both</label>
                                  <span class="text-danger"id="i_am_a_err" style="color:red;"></span>
                                </div>
                          </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="password"  name="password" class="form-control form-control-lg" placeholder="Password">
                                     <span class="text-danger"id="password_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                  <input type="password"  name="conf_pwd" class="form-control form-control-lg"
                                           placeholder="Confirm Password">
                                           <span class="text-danger"id="conf_pwd_err" style="color:red;"></span><br><br>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <input type="submit"   id="submit" class="btn btn-success btn-lg btn-block" value="Register Now">
                                <p class="forget-pass">A verification email wil be sent to you</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </main>
</div>
<!--/#app -->
<script src="assets/js/app.js"></script>
<script src="{{ URL::asset('//cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.js')}}"></script>
<script type="text/javascript">
 $.ajaxSetup({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $("#submit").click(function(e){
      e.preventDefault();

      var country_id = $("select[name=country_id]").val();
      var name = $("input[name=name]").val();
      var last_name = $("input[name=last_name]").val();
      var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
      var email = $("input[name=email]").val();
      var phone = $("input[name=phone]").val();
      var company_name = $("input[name=company_name]").val();
      var i_am_a = $("input:radio[name='i_am_a']:checked").val();
      var password =$("input[name=password]").val();
      var conf_pwd =$("input[name=conf_pwd]").val();
      var url = '{{ url('register') }}';
      var Url = '{{ url('validate_email') }}';
  
      if(country_id == null) {
        $("#country_err").html("Please Enter country Name .");
        return false;
      }
      else if(name == '') {
        $("#name_err").html("Please Enter First Name.");
        return false;
      } 
      else if(last_name == '') {
        $("#last_name_err").html("Please Enter Last Name .");
        return false;
      }
      else if(!filter.test(email)) {
        $("#email_err").html("Please specify a valid email address.");
        return false;
      }
      else if(phone== '') {
        $("#phone_err").html("Please Enter phone .");
        return false;
      }
      else if(company_name == '') {
        $("#company_name_err").html("Please Enter company Name .");
        return false;
      }
      else if(i_am_a == null) {
        $("#i_am_a_err").html("Please Enter  Your Identity .");
        return false;
      }
      else if(password == '') {
        $("#password_err").html("Please Enter Password .");
        return false;
      }
      else if(conf_pwd == '') {
        $("#conf_pwd_err").html("Please Enter Confirm Password.");
        return false;
      }

       else {

        $.ajax({
           url:url,
           method:'POST', 
           data:$('#add_form').serialize(),
           success:function(response){
            // Swal.fire({
            //       title: response.message,
            //       type: 'warning',
            //       showCloseButton: true
            //     })
            console.log(response.success)
              if(response.success==401)
              {
                 Swal.fire({
                  title: 'The email has already been taken.',
                  type: 'warning',
                  showCloseButton: true
                })
              }
           else if(response.success==false){
    
                    Swal.fire({
                    title: 'Passwords Do Not Matched',
                    type: 'warning',
                    showCloseButton: true
                })
                  }
            else if(response.success==true){
    
                    Swal.fire({
                    title: 'Data inserted successfully',
                    type: 'success',
                    showCloseButton: true
                })
                     $('.btn-submit')[0].reset();
                       location.href = 'index';
                  }
            else{
                Swal.fire({
                title: 'Unable to enter',
                type: 'error',
                showCloseButton: true
                })
          
                  }

           },

        });
       
      }



  });


</script>
</body>
</html>