@include('include.header')
@include('include.sidebar')

<div class="page has-sidebar-left">
    <div>
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <div class="pb-3">
                            <div class="image mr-3  float-left">
                                <img class="user_avatar no-b no-p" src="{{url('assets/img/dummy/u6.png')}}" alt="User Image">
                            </div>
                            <div>
                                <h6 class="p-t-10">Alexander Pierce</h6>
                                alexander@paper.com
                            </div>
                        </div>
                    </div>
                </div>

              <div class="row">
                  <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                      <li>
                          <a class="nav-link" id="v-pills-home-tab" data-toggle="pill" href="" role="tab" aria-controls="v-pills-home" aria-selected="false"><i class="icon icon-home2"></i>Home</a>
                      </li>
                      <li>
                          <a class="nav-link" id="v-pills-payments-tab" data-toggle="pill" href="#v-pills-payments" role="tab" aria-controls="v-pills-payments" aria-selected="false"><i class="icon icon-money-1"></i>Payments</a>
                      </li>
                      <li>
                          <a class="nav-link" id="v-pills-timeline-tab" data-toggle="pill" href="#v-pills-timeline" role="tab" aria-controls="v-pills-timeline" aria-selected="false"><i class="icon icon-cog"></i>Timeline</a>
                      </li>
                      <li>
                          <a class="nav-link active" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="true"><i class="icon icon-cog"></i>Edit Profile</a>
                      </li>
                  </ul>
              </div>

            </div>
        </header>

        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort go">
           <div class="tab-content" id="v-pills-tabContent">
               <div class="tab-pane fade" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                   <div class="row">
                       <div class="col-md-3">
                           <div class="card ">

                               <ul class="list-group list-group-flush">
                                   <li class="list-group-item"><i class="icon icon-mobile text-primary"></i><strong class="s-12">Phone</strong> <span class="float-right s-12">+91 333470 456 99</span></li>
                                   <li class="list-group-item"><i class="icon icon-mail text-success"></i><strong class="s-12">Email</strong> <span class="float-right s-12">abc@paper.com</span></li>
                                   <li class="list-group-item"><i class="icon icon-address-card-o text-warning"></i><strong class="s-12">Address</strong> <span class="float-right s-12">New York, USA</span></li>
                                   <li class="list-group-item"><i class="icon icon-web text-danger"></i> <strong class="s-12">Website</strong> <span class="float-right s-12">pappertemplate.com</span></li>
                               </ul>
                           </div>
                           <div class="card mt-3 mb-3">
                               <div class="card-header bg-white">
                                   <strong class="card-title">Guardian</strong>

                               </div>
                               <ul class="no-b">
                                   <li class="list-group-item">
                                       <a href="">
                                           <div class="image mr-3  float-left">
                                               <img class="user_avatar" src="{{('assets/img/dummy/u3.png')}}" alt="User Image">
                                           </div>
                                           <h6 class="p-t-10">Alexander Pierce</h6>
                                           <span><i class="icon-mobile-phone"></i>+92 333470963</span>
                                       </a>
                                   </li>
                               </ul>

                               <div class="card-header bg-white">
                                   <strong class="card-title">Siblings</strong>
                               </div>
                               <div>
                                   <ul class="list-group list-group-flush">
                                       <li class="list-group-item">
                                           <div class="image mr-3  float-left">
                                               <img class="user_avatar" src="assets/img/dummy/u1.png" alt="User Image">
                                           </div>
                                           <h6 class="p-t-10">Alexander Pierce</h6>
                                           <span> 4th Grade</span>
                                       </li>
                                       <li class="list-group-item">
                                           <div class="image mr-3  float-left">
                                               <img class="user_avatar" src="assets/img/dummy/u2.png" alt="User Image">
                                           </div>
                                           <h6 class="p-t-10">Alexander Pierce</h6>
                                           <span> 5th Grade</span>
                                       </li>
                                       <li class="list-group-item">
                                           <div class="image mr-3  float-left">
                                               <img class="user_avatar" src="assets/img/dummy/u5.png" alt="User Image">
                                           </div>
                                           <h6 class="p-t-10">Alexander Pierce</h6>
                                           <span> 6th Grade</span>
                                       </li>
                                       <li class="list-group-item">
                                           <div class="image mr-3  float-left">
                                               <img class="user_avatar" src="assets/img/dummy/u4.png" alt="User Image">
                                           </div>
                                           <h6 class="p-t-10">Alexander Pierce</h6>
                                           <span> 10th Grade</span>
                                       </li>
                                   </ul>
                               </div>

                           </div>

                       </div>
                       <div class="col-md-9">
                        
                           <div class="row">
                               <div class="col-lg-4">
                                   <div class="card r-3">
                                       <div class="p-4">
                                           <div class="float-right">
                                               <span class="icon-award text-light-blue s-48"></span>
                                           </div>
                                           <div class="counter-title">Class Position</div>
                                           <h5 class="sc-counter mt-3 counter-animated">5</h5>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-lg-4">
                                   <div class="card r-3">
                                       <div class="p-4">
                                           <div class="float-right"><span class="icon-stop-watch3 s-48"></span>
                                           </div>
                                           <div class="counter-title ">Absence</div>
                                           <h5 class="sc-counter mt-3 counter-animated">12</h5>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-lg-4">
                                   <div class="white card">
                                       <div class="p-4">
                                           <div class="float-right"><span class="icon-orders s-48"></span>
                                           </div>
                                           <div class="counter-title">Roll Number</div>
                                           <h5 class="sc-counter mt-3 counter-animated">26</h5>
                                       </div>
                                   </div>
                               </div>
                           </div>

                           <div class="row my-3">
                               <!-- bar charts group -->
                               <div class="col-md-12">
                                   <div class="card">
                                       <div class="card-header white">
                                           <h6>Attendance <small>Sessions</small></h6>
                                       </div>
                                       <div class="card-body">
                                           <div id="graphx" style="width: 100%; height: 300px; position: relative; -webkit-tap-highlight-color: rgba(0, 0, 0, 0);"><svg height="300" version="1.1" width="737" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="overflow: hidden; position: relative; left: -0.75px;"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.2.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><text x="12.5" y="275" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="275" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">0</tspan></text><path fill="none" stroke="#aaaaaa" d="M25,275.5H-25" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="12.5" y="212.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="212.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">2</tspan></text><path fill="none" stroke="#aaaaaa" d="M25,212.5H-25" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="12.5" y="150" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="150" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">4</tspan></text><path fill="none" stroke="#aaaaaa" d="M25,150.5H-25" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="12.5" y="87.5" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="87.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">6</tspan></text><path fill="none" stroke="#aaaaaa" d="M25,87.5H-25" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><text x="12.5" y="25" text-anchor="end" font-family="sans-serif" font-size="12px" stroke="none" fill="#888888" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); text-anchor: end; font-family: sans-serif; font-size: 12px; font-weight: normal;" font-weight="normal"><tspan dy="25" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">8</tspan></text><path fill="none" stroke="#aaaaaa" d="M25,25.5H-25" stroke-width="0.5" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><rect x="25.03125" y="212.5" width="-1.9375" height="62.5" rx="0" ry="0" fill="#2979ff" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="26.09375" y="181.25" width="-1.9375" height="93.75" rx="0" ry="0" fill="#34495e" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="27.15625" y="150" width="-1.9375" height="125" rx="0" ry="0" fill="#acadac" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="25.28125" y="181.25" width="-1.9375" height="93.75" rx="0" ry="0" fill="#2979ff" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="26.34375" y="118.75" width="-1.9375" height="156.25" rx="0" ry="0" fill="#34495e" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="27.40625" y="87.5" width="-1.9375" height="187.5" rx="0" ry="0" fill="#acadac" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="25.53125" y="150" width="-1.9375" height="125" rx="0" ry="0" fill="#2979ff" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="26.59375" y="181.25" width="-1.9375" height="93.75" rx="0" ry="0" fill="#34495e" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="27.65625" y="212.5" width="-1.9375" height="62.5" rx="0" ry="0" fill="#acadac" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="25.78125" y="212.5" width="-1.9375" height="62.5" rx="0" ry="0" fill="#2979ff" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="26.84375" y="150" width="-1.9375" height="125" rx="0" ry="0" fill="#34495e" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect><rect x="27.90625" y="118.75" width="-1.9375" height="156.25" rx="0" ry="0" fill="#acadac" stroke="none" fill-opacity="1" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0); fill-opacity: 1;"></rect></svg><div class="morris-hover morris-default-style" style="display: none;"></div></div>
                                       </div>
                                   </div>
                               </div>
                               /bar charts group


                           </div>
                           <div class="row">
                               <div class="col-md-12">
                                   <div class="card">
                                       <div class="card-header white">
                                           <h6>New Followers <span class="badge badge-success r-3">30+</span></h6>
                                       </div>
                                       <div class="card-body">

                                           <ul class="list-inline mt-3">
                                               <li class="list-inline-item ">
                                                   <img src="assets/img/dummy/u13.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u12.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u11.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u10.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u9.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u8.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item ">
                                                   <img src="assets/img/dummy/u7.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u6.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u5.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u4.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u3.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                               <li class="list-inline-item">
                                                   <img src="assets/img/dummy/u2.png" alt="" class="img-responsive w-40px circle mb-3">
                                               </li>
                                           </ul>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div> 
               </div>
               <div class="tab-pane fade" id="v-pills-payments" role="tabpanel" aria-labelledby="v-pills-payments-tab">
                   <div class="row">
                       <div class="col-md-12">
                           <div class="card no-b">
                               <div class="card-header white b-0 p-3">
                                   <h4 class="card-title">Invoices</h4>
                                   <small class="card-subtitle mb-2 text-muted">Items purchase by users.</small>
                               </div>
                               <div class="collapse show" id="invoiceCard">
                                   <div class="card-body p-0">
                                       <div class="table-responsive">
                                           <table id="recent-orders" class="table table-hover mb-0 ps-container ps-theme-default">
                                               <thead class="bg-light">
                                               <tr>
                                                   <th>SKU</th>
                                                   <th>Invoice#</th>
                                                   <th>Customer Name</th>
                                                   <th>Status</th>
                                                   <th>Amount</th>
                                               </tr>
                                               </thead>
                                               <tbody>
                                               <tr>
                                                   <td>PAP-10521</td>
                                                   <td><a href="#">INV-281281</a></td>
                                                   <td>Baja Khan</td>
                                                   <td><span class="badge badge-success">Paid</span></td>
                                                   <td>$ 1228.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-532521</td>
                                                   <td><a href="#">INV-01112</a></td>
                                                   <td>Khan Sab</td>
                                                   <td><span class="badge badge-warning">Overdue</span>
                                                   </td>
                                                   <td>$ 5685.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-05521</td>
                                                   <td><a href="#">INV-281012</a></td>
                                                   <td>Bin Ladin</td>
                                                   <td><span class="badge badge-success">Paid</span></td>
                                                   <td>$ 152.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-15521</td>
                                                   <td><a href="#">INV-281401</a></td>
                                                   <td>Zoor Shoor</td>
                                                   <td><span class="badge badge-success">Paid</span></td>
                                                   <td>$ 1450.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-532521</td>
                                                   <td><a href="#">INV-01112</a></td>
                                                   <td>Khan Sab</td>
                                                   <td><span class="badge badge-warning">Overdue</span>
                                                   </td>
                                                   <td>$ 5685.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-05521</td>
                                                   <td><a href="#">INV-281012</a></td>
                                                   <td>Bin Ladin</td>
                                                   <td><span class="badge badge-success">Paid</span></td>
                                                   <td>$ 152.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-15521</td>
                                                   <td><a href="#">INV-281401</a></td>
                                                   <td>Zoor Shoor</td>
                                                   <td><span class="badge badge-success">Paid</span></td>
                                                   <td>$ 1450.28</td>
                                               </tr>
                                               <tr>
                                                   <td>PAP-32521</td>
                                                   <td><a href="#">INV-288101</a></td>
                                                   <td>Walter R.</td>
                                                   <td><span class="badge badge-warning">Overdue</span>
                                                   </td>
                                                   <td>$ 685.28</td>
                                               </tr>
                                               </tbody>
                                           </table>
                                       </div>
                                   </div>
                               </div>
                           </div>
                       </div>
                   </div>

               </div>
               <div class="tab-pane fade" id="v-pills-timeline" role="tabpanel" aria-labelledby="v-pills-timeline-tab">

                   <div class="row">
                       <div class="col-md-12">
                           <!-- The time line
                           <ul class="timeline">
                               <!-- timeline time label -->
                               <li class="time-label">
                  <span class="badge badge-danger r-3">
                    10 Feb. 2014
                  </span>
                               </li>
                               <!-- /.timeline-label -->
                               <!-- timeline item -->
                               <li>
                                   <i class="ion icon-envelope bg-primary"></i>
                                   <div class="timeline-item card">
                                       <div class="card-header white"><a href="#">Support Team</a> sent you an email    <span class="time float-right"><i class="ion icon-clock-o"></i> 12:05</span></div>
                                       <div class="card-body">
                                           Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
                                           weebly ning heekya handango imeem plugg dopplr jibjab, movity
                                           jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
                                           quora plaxo ideeli hulu weebly balihoo...
                                       </div>
                                       <div class="card-footer">
                                           <a class="btn btn-primary btn-xs">Read more</a>
                                           <a class="btn btn-danger btn-xs">Delete</a>
                                       </div>
                                   </div>
                               </li>
                               <!-- END timeline item -->
                               <!-- timeline item -->
                               <li>
                                   <i class="ion icon-user yellow"></i>

                                   <div class="timeline-item  card">

                                       <div class="card-header white"><h6><a href="#">Sarah Young</a> accepted your friend request<span class="float-right"><i class="ion icon-clock-o"></i> 5 mins ago</span></h6></div>


                                   </div>
                               </li>
                               <!-- END timeline item -->
                               <!-- timeline item -->
                               <li>
                                   <i class="ion icon-comments bg-danger"></i>

                                   <div class="timeline-item  card">


                                       <div class="card-header white"><h6><a href="#">Jay White</a> commented on your post   <span class="float-right"><i class="ion icon-clock-o"></i> 27 mins ago</span></h6></div>

                                       <div class="card-body">
                                           Take me to your leader!
                                           Switzerland is small and neutral!
                                           We are more like Germany, ambitious and misunderstood!
                                       </div>
                                       <div class="card-footer">
                                           <a class="btn btn-warning btn-flat btn-xs">View comment</a>
                                       </div>
                                   </div>
                               </li>
                               <!-- END timeline item -->
                               <!-- timeline time label -->
                               <li class="time-label">
                  <span class="badge badge-success r-3">
                    3 Jan. 2014
                  </span>
                               <!-- </li> -->
                               <!-- /.timeline-label -->
                               <!-- timeline item -->
                               <li>
                                   <i class="ion icon-camera indigo"></i>

                                   <div class="timeline-item  card">

                                       <div class="card-header white"><a href="#">Mina Lee</a> uploaded new photos<span class="time float-right"><i class="ion icon-clock-o"></i> 2 days ago</span></div>


                                       <div class="card-body">
                                           <img src="http://placehold.it/150x100" alt="..." class="margin">
                                           <img src="http://placehold.it/150x100" alt="..." class="margin">
                                           <img src="http://placehold.it/150x100" alt="..." class="margin">
                                           <img src="http://placehold.it/150x100" alt="..." class="margin">
                                       </div>
                                   </div>
                               </li>
                               <!-- END timeline item -->
                               <!-- timeline item -->
                               <li>
                                   <i class="ion icon-video-camera bg-maroon"></i>

                                   <div class="timeline-item  card">
                                       <div class="card-header white"><a href="#">Mr. Doe</a> shared a video<span class="time float-right"><i class="ion icon-clock-o"></i> 5 days ago</span></div>


                                       <div class="card-body">
                                           <div class="embed-responsive embed-responsive-16by9">
                                               <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/tMWkeBIohBs" allowfullscreen="" frameborder="0"></iframe>
                                           </div>
                                       </div>
                                       <div class="card-footer">
                                           <a href="#" class="btn btn-xs bg-maroon">See comments</a>
                                       </div>
                                   </div>
                               </li>
                               <!-- END timeline item -->
                               <li>
                                   <i class="ion icon-clock-o bg-gray"></i>
                               </li>
                           </ul>
                       </div>
                       <!-- /.col -->
                   </div>
               </div>
               <div class="tab-pane fade active show" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">
                  <form class="form-horizontal" method="post">
                  @csrf
                     <input type="hidden" name="id" value="<?php echo $row['id']; ?>">
                    <div class="form-group">
                           <label for="inputName" class="col-sm-2 control-label">Name</label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputName" name="name" value="<?php echo $row['name']; ?>" type="text">
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="inputName" class="col-sm-2 control-label"> Last Name</label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputName" name="last_name" value="<?php echo $row['last_name']; ?>" type="text">
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="inputEmail" class="col-sm-2 control-label">Email</label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputEmail" name="email" value="<?php echo $row['email']; ?>" type="email">
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="inputName" class="col-sm-2 control-label">Phone</label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputName" name="phone" value="<?php echo $row['phone']; ?>" type="text">
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="inputExperience" class="col-sm-2 control-label">company Name</label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputName" name="company_name" value="<?php echo $row['company_name']; ?>" type="text">
                           </div>
                       </div>
                       <div class="form-group">
                           <label for="inputSkills" class="col-sm-2 control-label"></label>

                           <div class="col-sm-10">
                               <input class="form-control" id="inputSkills" value="<?php echo $row['i_am_a']; ?>" name="i_am_a" type="text">
                                  <input type="radio" id="supplier" name="i_am_a" value="supplier">
                                  <label for="Supplier" <?php if ($row['i_am_a'] == 'supplier') echo 'selected' ; ?>>Supplier</label>

                                <input type="radio" id="buyer" name="i_am_a" value="buyer">
                                  <label for="buyer" <?php if ($row['i_am_a'] == 'buyer') echo 'selected' ; ?>>Buyer</label>

                                  <input type="radio" id="both" name="i_am_a" value="both">
                                  <label for="both" <?php if ($row['i_am_a'] == 'both') echo 'selected' ; ?>>Both</label>
                                  <span class="text-danger"id="i_am_a_err" style="color:red;"></span>
                            </div>
                       <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                               <div class="checkbox">
                                   <label>
                                       <input type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                   </label>
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <div class="col-sm-offset-2 col-sm-10">
                               <button type="submit" name="update" class="btn btn-danger">Update</button>
                           </div>
                       </div>
                    </div>
                </form>
           </div>
       </div>
     </div>

    </div>
</div>
</div>

                
@include('include.footer')


