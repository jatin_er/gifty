<div class="card-footer white">
    <!-- Chat button -->
    <form>
    <div class="input-group">
    <input class="form-control s-12 bg-light r-30 mr-3"
    placeholder="Type your message..." type="text">
    <span class="input-group-btn">
            <button type="submit" class="btn-fab btn-danger p-0 s-14"><i
                    class="icon-subdirectory_arrow_left"></i></button>
        </span>
    </div>
    </form>
    </div>


<script src="{{ asset('/assets/js/app.js') }}"></script>
<script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
