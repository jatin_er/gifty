<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminPress;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


		//Login///
 Route::view('index','index')->middleware('auth');
 Route::view('login','login');
 // Route::get('/', [AdminPress::class, 'index']);
 Route::post('login',[AdminPress::class, 'login'])->name('login');
		//Register//
Route::view('register','register');
Route::get('/', [AdminPress::class, 'register']);
Route::post('/register', [AdminPress::class, 'register']);
Route::get('/register', [AdminPress::class, 'countries']);
		//Vendor Listing//
Route::view('vendorList','vendorList');
Route::get('/vendorList', [AdminPress::class, 'vendorList']);
		//Pagination//
Route::get('/vendorList', [AdminPress::class, 'pagination']);
Route::get('vendorList/fetch_data',[AdminPress::class, 'fetch_data']);
			//Edit Vendor/ Udate Vendor /Delete Vendor//
Route::view('editVendor','editVendor');
Route::get('/editVendor/{id}',[AdminPress::class, 'editVendor']);
Route::post('/editVendor/{id}', [AdminPress::class, 'editVendorUpdate']);
Route::get('deleteVendor/{id}', [AdminPress::class, 'deleteVendor']);

