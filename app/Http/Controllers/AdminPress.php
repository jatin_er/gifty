<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\UsersModel;
use App\Models\countries;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;



class AdminPress extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
    }

    // public function index()
    // {
    //   return view('index');

    // }
    public function countries()
    {
        $countries = countries::all();
        return view('register',['countries_data'=>$countries]);
    }
    public function register(Request $request)
    {   
        
        // $this->Validate($request,[
        //     'email'=>'required|unique:users'
        // ]);
        // return response()->json(['success'=>false, 'message' => 'The email has already been taken.']);

        $UsersModel = new UsersModel;

        $UsersModel->country_id=$request->country_id;
        $UsersModel->name=$request->name;
        $UsersModel->last_name=$request->last_name;
        $UsersModel->email=$request->email;
        $UsersModel->phone=$request->phone;
        $UsersModel->company_name=$request->company_name;
        $UsersModel->i_am_a=$request->i_am_a;

        // $requestData = $request->all();
        $user = UsersModel::where('email', '=' , $request['email'])->first();
            if (!empty($user)) {
            return response()->json(['success'=>401, 'message' => 'The email has already been taken.']);
            }
               // print_r($user);die;
       if($request->password == $request->conf_pwd){
            $UsersModel->password=Hash::make('password');
             $UsersModel->save();
            }
       
        else{
            return response()->json(
            [
                'success' => false,
                'message' => 'Passwords Do Not Matched.'
            ]
            );
        }
    
        return response()->json(
        [
            'success' => true,
            'message' => 'Data inserted successfully'
        ]
        );
        
    }
             
     public function login(Request $request)
    {   //print_r(($request->all()); 
        
         $requestData = $request->all();
        $user = UsersModel::where('email', '=' , $requestData['email'])
                    ->first();
        if($user){
            if (Auth::loginUsingId($user->id)) {
              // return redirect('/index');
                return response()->json(['success'=>true, 'message' => 'Login successfully']);
            }else{
                // return redirect('/login');
                return response()->json(['success'=>false, 'message' => 'Login Fail, pls check password']);

            }    
        }else{
            // return redirect('/login');
            return response()->json(['success'=>false, 'message' => ' Unable Login']);

        } 

    }
    public function vendorList(Request $request)
    {   
        $data = UsersModel::all();
        return view('vendorList',['data'=>$data]);
   
    }
    public function pagination()
    {
     $data = UsersModel::orderBy('id','desc')->simplePaginate(5);
     return view('vendorList', compact('data'));
    }

    public function fetch_data(Request $request)
    {   
         if($request->ajax())
         {      
            $query = $request->get('query');
            $query = str_replace(" ", "%", $query);
            $data = UsersModel::where('id', 'like', '%'.$query.'%')
                        ->orWhere('name', 'like', '%'.$query.'%')
                        ->orWhere('email', 'like', '%'.$query.'%')
                        ->simplePaginate(5);
          return view('pagination_data', compact('data'))->render();
         }
    }
    public function editVendor($id)
    {   
        $editVendor = UsersModel::where('id', $id)->first();
         return view('editVendor',['row'=>$editVendor]);

    }

     public function editVendorUpdate($id, Request $request)
    {   
        $UsersModel  = UsersModel::where('id', $id)->first();
        if (is_null($UsersModel)) {
             return ['status' => 'fail'];
        }
        $UsersModel->name=$request->name;
        $UsersModel->last_name=$request->last_name;
        $UsersModel->email=$request->email;
        $UsersModel->phone=$request->phone;
        $UsersModel->company_name=$request->company_name;
        $UsersModel->i_am_a=$request->i_am_a;
        
        $UsersModel->save();
     

        return redirect('/vendorList');
    
    }
    public function deleteVendor($id)
    {
        $user = UsersModel::where('id',$id)->delete();
        return redirect('/vendorList');
    }   

    
    
    

}
