<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Admin;
use App\Models\User;
use App\Models\Catalog;
use App\Models\Subcatalog;
use App\Models\Brand;
use App\Models\Attribute;
use App\Models\Tax;
use App\Models\Zone;
use App\Models\Country;
use App\Models\State;
use App\Models\City;
use App\Models\Weight;
use App\Models\ShippingServices;
use App\Models\Product;
use Session;

class AdminController extends Controller
{
    public function index()
    {
        if(session()->has('admin_id'))
        {
            return redirect()->intended('admin/dashboard');
        }
        else
        {
            $data['title'] = "Login";
        	return view('admin.index',compact('data'));
        }
    }
    public function admin_loginCheck(Request $request)
    {
        $username = $request->input('admin_username');
        $pswrd = $request->input('admin_pswrd');
        $condition = array(
            'username' => $username,
            'password'     => md5($pswrd)
        );
        $adm = Admin::where($condition)->first();
        if($adm) 
        {
           	$request->session()->put('admin_id',$adm->admin_id);
            $request->session()->put('admin_name',$adm->admin_name);
            $request->session()->put('admin_email',$adm->admin_email);
            return ['status' => 'success', 'msg' =>'Successfully Login'];
        }
        else
        {
            return ['status' => 'fail', 'msg' =>'Invalid Login'];
        }
    }
    public function admin_dashboard()
    {
        if(session()->has("admin_id"))
        {
            $data['title'] = "Dashboard";
            return view('admin.newdashboard',compact('data'));
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function users(Request $request)
    {
        if(session()->has("admin_id"))
        {
            $data['title']="Users";
            $user=User::where('deleted_at','0');
            if($request->has('role') || $request->has('search'))
            {
                $role=$request->get('role');
                $search=$request->get('search');
                if($role!="")
                {
                    $user=$user->where('i_am_a',$role);
                }
                if($search!="")
                {
                	$user=$user->where(function($q,$search) {
			        	$q->where('name','Like','%'.$search.'%')
			        	->orWhere('last_name','Like','%'.$search.'%')
			        	->orWhere('email','Like','%'.$search.'%')
			        	->orWhere('company_name','Like','%'.$search.'%')
			        	->orWhere('phone','Like','%'.$search.'%')
			        	->orWhere('i_am_a','Like','%'.$search.'%');
			      	});
                }
            }
            $user=$user->orderBy('id','ASC')->paginate(2);
            if ($request->ajax()) 
            {
            	return view('admin.users')->with(compact('data','user'));
            }
            return view('admin.users')->with(compact('data','user'));
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function category()
    {
        if(session()->has('admin_id'))
        {
            $data['title'] = "Category";
            return view('admin.category',compact('data'));
        }
        else
        {
            return redirect()->intended('/admin');
        }
    }
    public function add_category(Request $request)
    {   
		if($request->hasFile('image'))
		{
			$image=$request->file('image');
			$extension=strtolower($request->image->getClientOriginalExtension());
			if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
			{
				$po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
				$dir = 'public/product/catalog';
				$request->file('image')->move($dir, $po_logoname);
			}
			else
			{
				return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)'];
				$po_logoname = "";
			}
		}
		else
		{
			return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)bgh'];
			$po_logoname = "";
		}
		if($po_logoname!='')
		{
	    	$category=ucfirst($request->get('category'));
	    	date_default_timezone_set('Asia/Kolkata');
			$create_date=date("Y-m-d");
			$create_time=date("h:i:sa");
			$checkcategory=Catalog::where('cat_name',$category)->first();
			if($checkcategory)
			{
				return ['status' => 'exist', 'redirect' => '', 'msg' =>'Category already exist'];
			}
			else
			{
				$newsl=strtolower($request->get('category'));
				$slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $newsl);
				$checkcategorynew=Catalog::where('cat_slug',$slug)->first();
				if($checkcategorynew)
				{
					$newrand=rand(1,9);
					$slug=$slug.$newrand;
				}
				$maxValue = Catalog::max('cat_order');
				$neworder=$maxValue + 1;
				$maindataarr=array('cat_name' => $category,
									'cat_image'=>$po_logoname,
									'cat_slug'=>$slug,
									'seokey'=>$request->get('seokey'),
									'meta_description'=>$request->get('meta_description'),
									'cat_order'=>$neworder,
									'cat_status'=>'1',
									'cat_date'=>$create_date,
									'cat_time'=>$create_time,
								 );
				$datasave=Catalog::insert($maindataarr);
				if($datasave)
				{
					return ['status' => 'success', 'redirect' => '', 'msg' =>'Category Added Successfully'];
				}
				else
				{
					return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
				}
			}
		}
    }
    public function category_list(Request $request)
    {
        if(session()->has("admin_id"))
        {
            $query=Catalog::where('cat_status','1')->orderBy('cat_id', 'DESC');
	        if($request->has('search'))
	        {
	        	$search=$request->get('search');
	            $query->where(function ($query) use ($search) 
                {
                    $query->orWhere('cat_name', 'like' , "%".$search."%")
                    ->orWhere('cat_slug', 'like' , "%".$search."%")
                    ->orWhere('seokey', 'like' , "%".$search."%")
                    ->orWhere('meta_description', 'like' , "%".$search."%");
                });
	        }
            $cat=$query->paginate(5);
            $data['title']='Category List';
            return view('admin.category-list', compact('data','cat'))->render();
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function fetch_cat_data(Request $request)
    {   
        $cat=Catalog::where('cat_id',$request->get('cat_id'))->first();
        echo json_encode($cat);
    }
    public function edit_category(Request $request)
    {   
        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $extension=strtolower($request->image->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
            {
                $po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
                $dir = 'public/product/catalog';
                $request->file('image')->move($dir, $po_logoname);
            }
            else
            {
                return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)'];
                $po_logoname = "";
            }
        }
        else
        {
            $po_logoname = $request->get('imgda');
        }
        if($po_logoname!='')
        {
            $category=ucfirst($request->get('category'));
            date_default_timezone_set('Asia/Kolkata');
            $create_date=date("Y-m-d");
            $create_time=date("h:i:sa");
            $checkcategory=Catalog::where('cat_name',$category)->where('cat_id','!=',$request->get('category_id'))->first();
            if($checkcategory)
            {
                return ['status' => 'exist', 'redirect' => '', 'msg' =>'Category already exist','image'=>''];
            }
            else
            {
                $updatedata=array(
                    "cat_name"=>$category,
                    "cat_image"=>$po_logoname,
                    "seokey"=>$request->get('seokey'),
                    "meta_description"=>$request->get('meta_description'),
                );
                $datasave = Catalog::where('cat_id',$request->category_id)->update($updatedata);
                if($datasave)
                {
                    return ['status' => 'success', 'redirect' => '', 'msg' =>'Category Updated Successfully','image'=>$po_logoname];
                }
                else
                {
                    return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while updation','image'=>''];
                }
            }
        }
    }
    public function sub_category()
    {
        if(session()->has('admin_id'))
        {
            $data['title'] = "Sub Category";
            $productdata=Catalog::where('cat_status','1')->orderBy('cat_name','ASC')->get();
            return view('admin.sub-category',compact('data','productdata'));
        }
        else
        {
            return redirect()->intended('/admin');
        }
    }
    public function add_subcategory(Request $request)
    {   
    	$subcategory=ucfirst($request->get('subcategory'));
    	$category=$request->get('category');
    	$seokey=$request->get('seokey');
    	$meta_description=$request->get('meta_description');
    	date_default_timezone_set('Asia/Kolkata');
		$create_date=date("Y-m-d");
		$create_time=date("h:i:sa");
		$maxValue = Subcatalog::max('scat_order');
		$neworder=$maxValue + 1;
		 $slugval=strtolower($request->get('subcategory'));
		 $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $slugval);
		 $checkcategory=Subcatalog::where('scat_name',$subcategory)->where('cat_id',$category)->first();
		if($checkcategory)
		{
			return ['status' => 'exist', 'redirect' => '', 'msg' =>'Subcategory already exist'];
		}
		else
		{
			$checkcategorynew=Subcatalog::where('scat_slug',$slug)->first();
			if($checkcategorynew)
			{
				$newrand=rand(1,9);
				$slug=$slug.$newrand;
			}
			$maindataarr=array('cat_id' => $category,
							'scat_name'=>$subcategory,
							'scat_slug'=>$slug,
							'seokey'=>$request->get('seokey'),
							'meta_description'=>$request->get('meta_description'),
							'scat_order'=>$neworder,
							'scat_status'=>'1',
							'scat_date'=>$create_date,
							'scat_time'=>$create_time,
						 );
			$datasave=Subcatalog::insert($maindataarr);
			if($datasave)
			{
				return ['status' => 'success', 'redirect' => '', 'msg' =>'Subcategory Added Successfully'];
			}
			else
			{
				return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
			}
		}
    }
    public function subcategory_list(Request $request)
    {
        if(session()->has("admin_id"))
        {
            $query=Subcatalog::leftJoin('catalogs','catalogs.cat_id','=','subcatalogs.cat_id')->where('subcatalogs.scat_status','1')->orderBy('subcatalogs.scat_id', 'DESC');
            if($request->has('search') || $request->has('cat'))
            {
                $search=$request->get('search');
                $cat=$request->get('cat');
                if($cat!='')
                {
                    $query->where('subcatalogs.cat_id',$cat);
                }
                if($search!='')
                {
                    $query->where(function ($query) use ($search) 
                    {
                        $query->orWhere('catalogs.cat_name', 'like' , "%".$search."%")
                        ->orWhere('subcatalogs.scat_name', 'like' , "%".$search."%")
                        ->orWhere('subcatalogs.scat_slug', 'like' , "%".$search."%")
                        ->orWhere('subcatalogs.seokey', 'like' , "%".$search."%")
                        ->orWhere('subcatalogs.meta_description', 'like' , "%".$search."%");
                    });
                }
            }
            $scat=$query->paginate(5);
            $productdata=Catalog::where('cat_status','1')->orderBy('cat_name','ASC')->get();
            $data['title']='Sub Category List';
            return view('admin.subcategory-list', compact('data','scat','productdata'))->render();
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function fetch_scat_data(Request $request)
    {   
        $scat=Subcatalog::where('scat_id',$request->get('scat_id'))->first();
        echo json_encode($scat);
    }
    public function edit_subcategory(Request $request)
    {   
        $subcategory=ucfirst($request->get('subcategory'));
        $category=$request->get('category');
        $scat_id=$request->get('scat_id');
        $seokey=$request->get('seokey');
        $meta_description=$request->get('meta_description');
        date_default_timezone_set('Asia/Kolkata');
        $create_date=date("Y-m-d");
        $create_time=date("h:i:sa");
        $checksubcategory=Subcatalog::where('scat_name',$subcategory)->where('cat_id',$category)->where('scat_id','!=',$scat_id)->first();
        if($checksubcategory)
        {
            return ['status' => 'exist', 'redirect' => '', 'msg' =>'Subcategory already exist'];
        }
        else
        {
            $updatedata=array(
                    "cat_id"=>$category,
                    "scat_name"=>$subcategory,
                    "seokey"=>$request->get('seokey'),
                    "meta_description"=>$request->get('meta_description'),
                );
            $datasave = Subcatalog::where('scat_id',$request->scat_id)->update($updatedata);
            if($datasave)
            {
                return ['status' => 'success', 'redirect' => '', 'msg' =>'Subcategory Updated Successfully'];
            }
            else
            {
                return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while updation'];
            }
        }
    }
    public function brand()
    {
        if(session()->has('admin_id'))
        {
            $data['title'] = "Brand";
            return view('admin.brand',compact('data'));
        }
        else
        {
            return redirect()->intended('/admin');
        }
    }
    public function add_brand(Request $request)
    {   
        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $extension=strtolower($request->image->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
            {
                $po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
                $dir = 'public/product/brand';
                $request->file('image')->move($dir, $po_logoname);
            }
            else
            {
                return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)'];
                $po_logoname = "";
            }
        }
        else
        {
            return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)bgh'];
            $po_logoname = "";
        }
        if($po_logoname!='')
        {
            $brand=ucfirst($request->get('brand'));
            date_default_timezone_set('Asia/Kolkata');
            $create_date=date("Y-m-d");
            $create_time=date("h:i:sa");
            $checkbrand=Brand::where('brand_name',$brand)->first();
            if($checkbrand)
            {
                return ['status' => 'exist', 'redirect' => '', 'msg' =>'Brand already exist'];
            }
            else
            {
                $newsl=strtolower($request->get('brand'));
                $slug=preg_replace('/[^A-Za-z0-9-]+/', '-', $newsl);
                $checkbrandnew=Brand::where('brand_slug',$slug)->first();
                if($checkbrandnew)
                {
                    $newrand=rand(1,9);
                    $slug=$slug.$newrand;
                }
                $maxValue = Brand::max('brand_order');
                $neworder=$maxValue + 1;
                $maindataarr=array('brand_name' => $brand,
                                    'brand_image'=>$po_logoname,
                                    'brand_slug'=>$slug,
                                    'seokey'=>$request->get('seokey'),
                                    'meta_description'=>$request->get('meta_description'),
                                    'brand_order'=>$neworder,
                                    'brand_status'=>'1',
                                    'brand_date'=>$create_date,
                                    'brand_time'=>$create_time,
                                 );
                $datasave=Brand::insert($maindataarr);
                if($datasave)
                {
                    return ['status' => 'success', 'redirect' => '', 'msg' =>'Brand Added Successfully'];
                }
                else
                {
                    return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
                }
            }
        }
    }
    public function brand_list(Request $request)
    {
        if(session()->has("admin_id"))
        {
            $query=Brand::where('brand_status','1')->orderBy('brand_id', 'DESC');
            if($request->has('search'))
            {
                $search=$request->get('search');
                $query->where(function ($query) use ($search) 
                {
                    $query->orWhere('brand_name', 'like' , "%".$search."%")
                    ->orWhere('brand_slug', 'like' , "%".$search."%")
                    ->orWhere('seokey', 'like' , "%".$search."%")
                    ->orWhere('meta_description', 'like' , "%".$search."%");
                });
            }
            $brand=$query->paginate(5);
            $data['title']='Brand List';
            return view('admin.brand-list', compact('data','brand'))->render();
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function fetch_brand_data(Request $request)
    {   
        $brand=Brand::where('brand_id',$request->get('brand_id'))->first();
        echo json_encode($brand);
    }
    public function edit_brand(Request $request)
    {   
        if($request->hasFile('image'))
        {
            $image=$request->file('image');
            $extension=strtolower($request->image->getClientOriginalExtension());
            if($extension=="png" || $extension=="jpg" || $extension=="jpeg")
            {
                $po_logoname = time().'.'.$request->file('image')->getClientOriginalExtension();
                $dir = 'public/product/brand';
                $request->file('image')->move($dir, $po_logoname);
            }
            else
            {
                return ['status' => 'chkimg', 'redirect' => '', 'msg' =>'Please check image(Only png, jpg, jpeg allowed)', 'image' =>''];
                $po_logoname = "";
            }
        }
        else
        {
            $po_logoname = $request->get('brand_imgs');
        }
        if($po_logoname!='')
        {
            $brand=ucfirst($request->get('brand'));
            $brand_id=$request->get('brand_id');
            date_default_timezone_set('Asia/Kolkata');
            $create_date=date("Y-m-d");
            $create_time=date("h:i:sa");
            $checkbrand=Brand::where('brand_name',$brand)->where('brand_id','!=',$brand_id)->first();
            if($checkbrand)
            {
                return ['status' => 'exist', 'redirect' => '', 'msg' =>'Brand already exist'];
            }
            else
            {
                $updatedata=array('brand_name' => $brand,
                                    'brand_image'=>$po_logoname,
                                    'seokey'=>$request->get('seokey'),
                                    'meta_description'=>$request->get('meta_description'),
                                 );
                $datasave = Brand::where('brand_id',$request->brand_id)->update($updatedata);
                if($datasave)
                {
                    return ['status' => 'success', 'redirect' => '', 'msg' =>'Brand Updated Successfully', 'image' =>$po_logoname];
                }
                else
                {
                    return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while updation', 'image' =>''];
                }
            }
        }
    }
    public function attribute()
    {
        if(session()->has('admin_id'))
        {
            $data['title'] = "Attribute";
            return view('admin.attribute',compact('data'));
        }
        else
        {
            return redirect()->intended('/login');
        }
    }
    public function add_attribute(Request $request)
    {   
        $attribute_name=ucfirst($request->get('attribute_name'));
        $attribute_type=$request->get('attribute_type');
        $attribute_selection=$request->get('attribute_selection');
        date_default_timezone_set('Asia/Kolkata');
        $create_date=date("Y-m-d");
        $create_time=date("h:i:sa");
        $checkattribute=Attribute::where('attribute_name',$attribute_name)->where('attribute_type',$attribute_type)->where('attribute_selection',$attribute_selection)->first();
        if($checkattribute)
        {
            return ['status' => 'exist', 'redirect' => '', 'msg' =>'Attribute already exist'];
        }
        else
        {
            $maxValue = Attribute::max('attribute_order');
            $neworder=$maxValue + 1;
            $maindataarr=array('attribute_name' => $attribute_name,
                                'attribute_type'=>$attribute_type,
                                'attribute_selection'=>$attribute_selection,
                                'attribute_order'=>$neworder,
                                'attribute_status'=>'1',
                                'attribute_date'=>$create_date,
                                'attribute_time'=>$create_time,
                             );
            $datasave=Attribute::insert($maindataarr);
            if($datasave)
            {
                return ['status' => 'success', 'redirect' => '', 'msg' =>'Attribute Added Successfully'];
            }
            else
            {
                return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while insertion'];
            }
        }
    }
    public function attribute_list(Request $request)
    {
        if(session()->has("admin_id"))
        {
            $query=Attribute::where('attribute_status','1')->orderBy('attribute_id', 'DESC');
            if($request->has('search'))
            {
                $search=$request->get('search');
                $query->where(function ($query) use ($search) 
                {
                    $query->orWhere('attribute_name', 'like' , "%".$search."%")
                    ->orWhere('attribute_type', 'like' , "%".$search."%")
                    ->orWhere('attribute_selection', 'like' , "%".$search."%");
                });
            }
            $attribute=$query->paginate(5);
            $data['title']='Attribute List';
            return view('admin.attribute-list', compact('data','attribute'))->render();
        }
        else
        {
            return redirect()->intended('admin');
        }
    }
    public function fetch_attribute_data(Request $request)
    {   
        $attribute=Attribute::where('attribute_id',$request->get('attribute_id'))->first();
        echo json_encode($attribute);
    }
    public function edit_attribute(Request $request)
    {   
        $attribute_name=ucfirst($request->get('attribute_name'));
        $attribute_type=$request->get('attribute_type');
        $attribute_selection=$request->get('attribute_selection');
        $attribute_id=$request->get('attribute_id');
        date_default_timezone_set('Asia/Kolkata');
        $create_date=date("Y-m-d");
        $create_time=date("h:i:sa");
        $checkattribute=Attribute::where('attribute_name',$attribute_name)->where('attribute_type',$attribute_type)->where('attribute_selection',$attribute_selection)->where('attribute_id','!=',$attribute_id)->first();
        if($checkattribute)
        {
            return ['status' => 'exist', 'redirect' => '', 'msg' =>'Attribute already exist'];
        }
        else
        {
            $updatedata=array('attribute_name' => $attribute_name,
                                'attribute_type'=>$attribute_type,
                                'attribute_selection'=>$attribute_selection,
                             );
            $datasave = Attribute::where('attribute_id',$request->attribute_id)->update($updatedata);
            if($datasave)
            {
                return ['status' => 'success', 'redirect' => '', 'msg' =>'Attribute Updated Successfully'];
            }
            else
            {
                return ['status' => 'fail', 'redirect' => '', 'msg' =>'Error while updation'];
            }
        }
    }
}
