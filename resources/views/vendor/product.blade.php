@include('vendor.include.header')
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    @include('vendor.include.menu')
    <!--Sidebar End-->
    <div class="has-sidebar-left">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                    <div class="search-bar">
                        <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                               placeholder="start typing...">
                    </div>
                    <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                       aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
                </div>
            </div>
        </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="nav-link " data-toggle="collapse" data-target="#navbarToggleExternalContent"
                               aria-controls="navbarToggleExternalContent"
                               aria-expanded="false" aria-label="Toggle navigation">
                                <i class=" icon-search3 "></i>
                            </a>
                        </li>
                        <!-- User Account-->
                        <li class="dropdown custom-dropdown user user-menu ">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <img src="{{ asset('vendor-assets/img/dummy/u8.png') }}" class="user-image" alt="User Image">
                                <i class="icon-more_vert "></i>
                            </a>
                            <div class="dropdown-menu p-4 dropdown-menu-right">
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Apps</div>
                                        </a>
                                    </div>
                                    <div class="col"><a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a></div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-star light-green lighten-1 avatar  r-5"></i>
                                            <div class="pt-1">Favourites</div>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-save2 orange accent-1 avatar  r-5"></i>
                                            <div class="pt-1">Saved</div>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting grey darken-3 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Apps</div>
                                        </a>
                                    </div>
                                    <div class="col"><a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a></div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            {{$data['title']}}
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="#"><i class="icon icon-list"></i>All Product</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                    class="icon icon-plus-circle"></i> Add New Product</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" class="product_form" novalidate enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <input type="hidden" name="upcseries" value="{{$newupc}}">
                                    <label for="product_title">Title*</label>
                                    <input type="text" class="form-control textfield" id="product_title" name="product_title" placeholder="Title" required autofocus>
                                    <span class="text-danger" id="product_title_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_short_description">Short Description</label>
                                        <textarea class="form-control" id="product_short_description" name="product_short_description" placeholder="Short Description" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="seokey">Meta Tag Keywords:comma separated</label>
                                    <input type="text" class="form-control" id="seokey" name="seokey" placeholder="Meta Tag Keywords:comma separated" required>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="metatag">Meta Tag</label>
                                        <input type="text" class="form-control" id="metatag" name="metatag" placeholder="Meta Tag" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_sku">Sku No</label>
                                        <input type="text" class="form-control" id="product_sku" name="product_sku" placeholder="Sku No" required/>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="metatag">Model No</label>
                                        <input type="text" class="form-control" id="product_model" name="product_model" placeholder="Model No" required/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_upc">UPC</label>
                                        <input type="text" class="form-control" id="product_upc" name="product_upc" placeholder="UPC" required readonly maxlength="12" value="{{$myupcno}}"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_category">Catalog*</label>
                                    <select id="product_category" name="product_category" class="custom-select form-control textfield" required>
                                        <option value="">Select Catalog</option>
                                        @foreach($productcat as $productcatlist)
                                        <option value="{{$productcatlist->cat_id}}">{{$productcatlist->cat_name}}</option>
                                        @endforeach
                                    </select>
                                    <span class="text-danger" id="product_category_err" style="color:red;"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="product_subcategory">Sub Catalog</label>
                                    <select id="product_subcategory" name="product_subcategory" class="custom-select form-control" required>
                                        <option value="">Select Sub Catalog</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_category1">Catalog Other</label>
                                    <select id="product_category1" name="product_category1" class="custom-select form-control" required>
                                        <option value="">Select Catalog</option>
                                        @foreach($productcat as $productcatlist)
                                        <option value="{{$productcatlist->cat_id}}">{{$productcatlist->cat_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="product_subcategory1">Sub Catalog Other</label>
                                    <select id="product_subcategory1" name="product_subcategory1" class="custom-select form-control" required>
                                        <option value="">Select Sub Catalog</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_brand">Brand</label>
                                    <select class="form-control select2" id="product_brand" name="product_brand[]" multiple="multiple" data-placeholder="Choose">
                                        @foreach($branddata as $brandlist)
                                        <option value="{{$brandlist->brand_id}}">{{$brandlist->brand_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="product_manfacture">Origin</label>
                                    <select id="product_manfacture" name="product_manfacture" class="custom-select form-control" required>
                                        <option value="">Select Origin</option>
                                        @foreach($country as $countr)
                                        <option value="{{$countr->name}}">{{$countr->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="">Product Images</label>
                                    <div class="row">
                                        <div class="col-md-12 mb-6">
                                            <div class="input-group-btn add1-1" id="1" value="1">
                                            <button type="button" class="btn btn-success  addbutton addval add-1 add-div"  id="1" value="1">
                                            Add More</button>
                                            </div>
                                        </div>
                                        <div class="col-md-12 mb-6 img-col">
                                            {{csrf_field()}}
                                            <div class="newimg"  id="newimg">
                                                <div class="custom-file pck" id="pck-1">
                                                    <input type="file" name="filename[]" class="custom-file-input pckgname filltext" id="pckimg-1" onchange=" return mainimageface();">
                                                    <label class="custom-file-label" for="customFile">Choose file</label>
                                                    &nbsp;<span id="bar-1" role="progressbar" style="width:0%;font-weight:bold;display:none; color:#88b93c;">0%</span>
                                                    <span style="color:red;visibility: hidden" id="pckimg_erroror-1" class="textfield_error pckimgerror" ></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="row img-row mt-5" id="img_div" style="display: none;">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_description">Description</label>
                                    <textarea class="form-control p-t-40 editor" id="product_description" name="product_description" placeholder="Write Something..." rows="17" required></textarea>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_related">Related Products</label>
                                    <select class="form-control select2" id="product_related" name="product_related[]" multiple="multiple" data-placeholder="Choose">
                                        @foreach($productdata as $prolist)
                                        <option value="{{$prolist->pro_id}}">{{$prolist->product_title}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_specfic">Types</label>
                                    <select class="form-control select2" id="product_specfic" name="product_specfic[]" multiple="multiple" data-placeholder="Choose">
                                        @foreach($itemspicific as $itemlistspec)
                                        <option value="{{$itemlistspec->attribute_id}}">{{$itemlistspec->attribute_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_specfic">Types</label>
                                    <select class="form-control select2" id="product_specfic" name="product_specfic[]" multiple="multiple" data-placeholder="Choose">
                                        @foreach($itemspicific as $itemlistspec)
                                        <option value="{{$itemlistspec->attribute_id}}">{{$itemlistspec->attribute_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row" id="showitemspecfic">
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <div class="form-group">
                                        <label for="product_data">Product Data</label>
                                        <select class="custom-select form-control" name="product_data" id="product_data">
                                        <option value="simple_product">Simple Product</option>
                                        <option value="variable_product">Variable Product</option>
                                       </select>
                                     </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-2 mb-1">
                                    <div class="input-group first-input">
                                        <label for="cost_price">Cost Price*</label>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control textfield" name="cost_price" id="cost_price">
                                        <span class="text-danger" id="cost_price_err" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <div class="form-group select-form">
                                        <label for="multi_price">Multiplier</label>
                                        <select class="custom-select form-control" name="multi_price" id="multi_price">
                                        @for($li=1;$li<11;$li++)
                                        <option value="{{$li}}">{{$li}}</option>
                                        @endfor
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <div class="input-group first-input input-1">
                                        <label for="sell_price" style="display: block;width:100% ">Selling Price*<span class="text-danger">*</span></label>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control textfield" name="sell_price" id="sell_price" readonly>
                                        <span class="text-danger" id="sell_price_err" style="color:red;"></span>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <div class="input-group first-input input-2">
                                        <label for="discount_price" style="display: block;width:100% ">Discount</label>
                                        <input type="text" class="form-control" name="discount_price" id="discount_price">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">%</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <div class="input-group first-input input-3">
                                        <label for="saving_price" style="display: block;width:100% ">Saving</label>
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control" name="saving_price" id="saving_price" readonly>
                                    </div>
                                </div>
                                <div class="col-md-2 mb-1">
                                    <div class="input-group input-4">
                                        <label for="regular_price">Discounted Price*</label>
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">$</span>
                                        </div>
                                        <input type="text" class="form-control textfield" name="regular_price" id="regular_price" readonly>
                                        <span class="text-danger" id="regular_price_err" style="color:red;"></span>
                                    </div>
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <label for="tax_price">Tax Class</label>
                                    <select class="form-control" name="tax_price" id="tax_price">
                                    <option value="">Select Tax Class</option>
                                    @foreach($producttax as $taxlist)
                                    <option value="{{$taxlist->tax_id}}">{{$taxlist->tax_name}}</option>
                                    @endforeach
                                    </select> 
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="product_inventory">Inventory*</label>
                                    <select class="form-control textfield" name="product_inventory" id="product_inventory">
                                        <option value="in_stock">In Stock</option>
                                        <option value="out_stock">Out of Stock</option>
                                    </select>
                                    <span class="text-danger" id="product_inventory_err" style="color:red;"></span>
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="quantity">Quantity*</label>
                                    <input type="number" class="form-control textfield" name="quantity" id="quantity" value="1">
                                    <span class="text-danger" id="quantity_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4 mb-2">
                                    <label for="product_condition">Conditions (This Product 'Brand New')</label>
                                    <input type="text" class="form-control" name="product_condition" id="product_condition">
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="product_warning">Warning</label>
                                    <input type="text" class="form-control" name="product_warning" id="product_warning">
                                </div>
                                <div class="col-md-4 mb-2">
                                    <label for="manfacture_date">Date*</label>
                                    <input type="text" class="date-time-picker form-control textfield" name="manfacture_date" id="manfacture_date" data-options='{"timepicker":false, "format":"d-m-Y"}'/>
                                    <span class="text-danger" id="quantity_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="product_reward">Reward</label>
                                    <input type="text" class="form-control" id="product_reward" name="product_reward" placeholder="Reward" required>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="product_weight">Shipping Weight</label>
                                    <select class="custom-select form-control" name="product_weight" id="product_weight">
                                    <option value="">Select weight</option>
                                        @foreach($weight as $weightlist)
                                        <option value="{{$weightlist->weight_id}}">{{$weightlist->weight_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row showweight">
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <h3 class="ship">Ship From</h3>
                                </div>
                                <div class="col-md-3 mb-2">
                                    <label for="country_name">Select Country </label>
                                    <select  name="country_name" id="country_name" class="custom-select form-control">
                                        <option value="">Select County</option>
                                        @foreach($country as $countr)
                                        <option value="{{$countr->id}}">{{$countr->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3 mb-2">
                                    <label for="state_name">Select State </label>
                                    <select  name="state_name" id="state_name" class="custom-select form-control">
                                        <option value="">Select State</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-2">
                                    <label for="city_name">Select City </label>
                                    <select  name="city_name" id="city_name" class="custom-select form-control">
                                        <option value="">Select City</option>
                                    </select>
                                </div>
                                <div class="col-md-3 mb-2">
                                    <input name="shipstatus" type="radio" id="radio_7" class="radio-col-blue" checked value="1" />
                                    <label for="radio_7" style="margin-top: 40px">Enable</label>
                                    <input name="shipstatus" type="radio" id="radio_8" class="radio-col-red"  value="0"/>
                                    <label for="radio_8" style="margin-top: 40px">Disable</label>
                                </div>
                            </div>
                            <div class="shipdata">
                                <div class="row myship ourship-1" id="1">
                                    <div class="col-md-3 mb-2">
                                        <label for="ship_services_1">Ship Services*</label>
                                        <select class="form-control textfield" name="ship_services[]" id="ship_services_1">
                                        @foreach($shipservice as $shipdata)
                                            <option value="{{$shipdata->ship_id}}" style="color:{{$shipdata->ship_color}}">{{$shipdata->ship_title}}</option>
                                        @endforeach
                                        </select>
                                        <span class="text-danger" id="ship_services_1_err" style="color:red;"></span>
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="ship_cost_1">Cost</label>
                                        <input type="text" class="form-control" name="ship_cost[]" id="ship_cost_1">
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="shipadditional">Each Additional </label>
                                        <input type="number" class="form-control shipadditional" name="shipadditional[]" id="shipadditional_1" value="0">
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="handling_time_1">Handling Time*</label>
                                        <select class="custom-select form-control textfield" name="handling_time[]" id="handling_time_1">
                                            <option value="Same Day">Same Day</option>
                                            <option value="1 to 2 days">1 to 2 days</option>
                                            <option value="2 to 3 days">2 to 3 days</option>
                                            <option value="3 to 4 days">3 to 4 days</option>
                                            <option value="4 to 5 days">4 to 5 days</option>
                                            <option value="5 to 6 days">5 to 6 days</option>
                                        </select>
                                        <span class="text-danger" id="handling_time_1_err"></span>
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <button type="button" class="btn btn-success  addshipbutton addshipval addship-1 addship-div"  id="1" value="1" style="margin-top:30px">
                                        Add</button>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <h3 class="ship">Discount</h3>
                            </div>
                            <div class="discountdata">
                                <div class="row mydis ourdis-1" id="1">
                                    <div class="col-md-2 mb-1">
                                        <label for="product_discount_1">Discount*</label>
                                        <select class="custom-select form-control textfield" name="product_discount[]" id="product_discount_1">
                                            <option value="flat_discount">Flat</option>
                                            <option value="per_discount" selected>%age</option>
                                        </select>
                                        <span class="text-danger" id="product_discount_1_err" style="color:red;"></span>
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="discount_qty_1">Enter Qty</label>
                                        <input type="text" class="form-control" name="discount_qty[]" id="discount_qty_1" value="3">
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="discount_1">Enter Discount</label>
                                        <input type="text" class="form-control discount" name="discount[]" id="discount_1" value="0">
                                    </div>
                                    <div class="col-md-2 mb-1">
                                        <label for="product_main_discount_1">Discount Price</label>
                                        <input type="text" class="form-control" name="product_main_discount[]" id="product_main_discount_1" value="0" readonly>
                                    </div>
                                    <div class="col-md-2">
                                        <button type="button" class="btn btn-success  adddisbutton adddisval adddis-1 adddis-div"  id="1" value="1" style="margin-top:30px">
                                        Add</button>
                                    </div>
                                    <input type="hidden" id="maindis_1" name="maindis" class="maindis">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="product_attribute">Attribute </label>
                                    <select class="form-control select2" name="product_attribute[]" id="product_attribute" multiple="multiple" data-placeholder="Choose Attribute">
                                        @foreach($proattiribute as $attributelist)
                                            <option value="{{$attributelist->attribute_id}}">{{$attributelist->attribute_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>  
                            <div class="row p-t-20" id="showattribute">
                            </div>
                            <div class="row p-t-20" id="showvariation"></div> 
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Publish Box</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="button" id="publish">Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php 
    $optionship="";
    foreach($shipservice as $shipdata)
    {
        $optionship.='<option value="'.$shipdata->ship_id.'" style="color:'.$shipdata->ship_color.'">'.$shipdata->ship_title.'</option>';
    }
?>   
<script src="{{ asset('vendor-assets/js/app.js') }}"></script>
<script>
    $(document).on('change','#product_category',function()
    {
        var id=$(this).val();
        $.ajax(
        {
            url:"{{route('search-subcategory')}}",
            data:{'id':id},
            type:'get',
            success:function(data)
            {
                $('#product_subcategory').html(data);
            }
        });
    });
    $(document).on('change','#product_category1',function()
    {
        var id=$(this).val();
        $.ajax(
        {
            url:"{{route('search-subcategory')}}",
            data:{'id':id},
            type:'get',
            success:function(data)
            {
                $('#product_subcategory1').html(data);
            }
        });
    });
    $(document).on('click','.addbutton',function()
    {
        var lastid1=$(this).val();
        var lastid=this.id;
        var newid=parseInt(lastid1) + 1;
        var fileName = $("#pckimg-"+lastid1).val();
        if(fileName == "") 
        {
            $('#pckimg_error-'+lastid1).text('Please Select Image');
            $('#pckimg_error-'+lastid1).css('visibility', 'visible');
        } 
        else
        {
            
            $('#newimg').append('<div class="pck custom-file" id="pck-'+newid+'"><input type="file" name="filename[]" class="custom-file-input pckgname filltext" id="pckimg-'+newid+'" onchange=" return mainimageface();"><label class="custom-file-label">Choose file</label>&nbsp;&nbsp;<span id="bar-'+newid+'" role="progressbar" style="width:0%;font-weight:bold;display:none; color:#88b93c;">0%</span><span style="color:red;visibility: hidden" id="pckimg_error-'+newid+'" class="textfield_error pckimgerror" ></span></div> <script>$(".custom-file-input").on("change", function() {var fileName = $(this).val().split("\\\\").pop();$(this).siblings(".custom-file-label").addClass("selected").html(fileName);});');
            $('.add-'+lastid).val(newid);
        }
    });
    function mainimageface() 
    {
        var chkid=$(".addval").val();
        var form_data = new FormData($('.product_form')[0]);
        form_data.append('_token', '{{csrf_token()}}');
        $('#bar-'+chkid).show();
        $.ajax(
        {
            url: '{{route("product_image_upload")}}',
            data: form_data,
            mimeType: "multipart/form-data",
            type: 'POST',
            contentType: false,
            processData: false,
            xhr: function () 
            {
                var xhr = new window.XMLHttpRequest();
                xhr.upload.addEventListener("progress", function (evt) {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total;
                        percentComplete = parseInt(percentComplete * 100);
                         $('#bar-'+chkid).text(percentComplete + '%');
                        $('#bar-'+chkid).css('width', percentComplete + '%');
                    }
                }, false);
                return xhr;
            },
            success: function (data) 
            {
                if(data=="no")
                {
                    $('#pckimg_error-'+chkid).text('Attachment should be JPEG|JPG|PNG|BMP|PDF');
                    $('#pckimg_error-'+chkid).css('visibility', 'visible');
                    $('#pckimg_error-'+chkid).focus();
                }
                else
                {
                    $('#pckimg_error-'+chkid).text('');
                    $('#pckimg_error-'+chkid).css('visibility', 'hidden');
                    $('#img_div').show();
                    var path='public/product_image'+data;
                    var newpath="{{asset('public/product_image')}}/"+data;
                    $("#pck_image-"+chkid).attr('src',"{{asset('public/product_image')}}/"+data);
                    $('#img_div').append('<div class="form-group col-md-3 col-sm-6" id="imgremove-'+chkid+'"><div class="over"><img src="'+newpath+'"  class="media-object"  id="pck_image-'+chkid+'" style="    height: 150px;object-fit: cover; margin: 0 auto; display: block;"></div><input type="hidden" id="packageup_image-'+chkid+'" name="packageup_image[]" class="packageup_image"><span class="close-img"><i class="fa fa-remove remove rem-'+chkid+'" id="'+chkid+'"  style="font-size:16px;color:#ffffff"></i></span></div>');
                   $('#packageup_image-'+chkid).val(data);
                }
            }
        });
    }
    $(document).on('click','.remove',function()
    {
        var lastid=this.id;
        $('#pck-'+lastid).remove();
        $('#imgremove-'+lastid).remove();
        if(lastid=="1")
        {
            $('.add-'+lastid).val(0);
        }
    });
    $(document).on('change','#product_weight',function()
    {
        var product_weight=$('#product_weight').val();
        if(product_weight=="0")
        {
            $('.showweight').html('');
            $('.showweight').hide();
        }
        else
        {
            $.ajax(
            {
                url:"{{route('product_weight_show')}}",
                data:{'product_weight':product_weight},
                type:'GET',
                success:function(data)
                {
                    $('.showweight').html(data);
                    $('.showweight').show();
                }
            });
        }
    });
    $(document).on('keyup change','#cost_price,#discount_price,#multi_price,#saving_price,#regular_price',function()
    {
        var cost_price=$('#cost_price').val();
        var multi_price=$('#multi_price').val();  
        var sell_price=$('#sell_price').val();
        var discount_price=$('#discount_price').val();
        if(cost_price=="")
        {
        var cost_price=0;
        }
        if(discount_price=="")
        {
        var discount_price=0;
        }
        var selprice=parseInt(cost_price) * parseInt(multi_price);
        var discountafter= (parseInt(selprice) * parseInt(discount_price)) / 100;
        $('#saving_price').val(discountafter);
        var regularprice=parseFloat(selprice) - parseFloat(discountafter);
        $('#regular_price').val(regularprice);
        // var newdiscont=parseInt(discountafter) % 100
        $('#maindis_1').val(regularprice);
        $('#sell_price').val(selprice);
        $('.variation_price').val(regularprice);
        $('#product_main_discount_1').val(regularprice);
    });
    $(document).on('change','#country_name',function()
    {        
        var country_id=$(this).val();
        if(country_id=="")
        {
            
        }
        else
        {
            $.ajax({
                url:"{{route('search_state')}}",
                data:{'country_id':country_id,},
                type:'GET',
                success:function(data)
                {
                    $('#state_name').html(data);
                }
            });
        }       
    });     
    $(document).on('change','#state_name',function()
    { 
        var state_name=$(this).val();
        if(state_name=="")
        {
            
        }
        else
        {
            $.ajax(
            {
                url:"{{route('search_city')}}",
                data:{'state_name':state_name,},
                type:'GET',
                success:function(data)
                {
                    $('#city_name').html(data);
                }
            });
        }
    });
    $(document).on('click','.addshipbutton',function()
    {
        var lastid1=$(this).val();
        var lastid=this.id;
        error=0;
        if(error==0)
        {
            var newid=parseInt(lastid1) + 1;
            var optionship='<?php echo $optionship;?>';
            $('.shipdata').append('<div class="row myship ourship-'+newid+'" id="'+newid+'"><div class="col-md-3 mb-2"><label for="ship_services_'+newid+'">Ship Services*</label><select class="form-control textfield" name="ship_services[]" id="ship_services_'+newid+'">'+optionship+'</select><span class="text-danger" id="ship_services_'+newid+'_err" style="color:red;"></span></div><div class="col-md-2 mb-1"><label for="ship_cost_'+newid+'">Cost</label><input type="text" class="form-control" name="ship_cost[]" id="ship_cost_'+newid+'"></div><div class="col-md-2 mb-1"><label for="shipadditional">Each Additional </label><input type="number" class="form-control shipadditional" name="shipadditional[]" id="shipadditional_'+newid+'" value="0"></div><div class="col-md-2 mb-1"><label for="handling_time_'+newid+'">Handling Time*</label><select class="custom-select form-control textfield" name="handling_time[]" id="handling_time_'+newid+'"><option value="Same Day">Same Day</option><option value="'+newid+' to 2 days">1 to 2 days</option><option value="2 to 3 days">2 to 3 days</option><option value="3 to 4 days">3 to 4 days</option><option value="4 to 5 days">4 to 5 days</option><option value="5 to 6 days">5 to 6 days</option></select><span class="text-danger" id="handling_time_'+newid+'_err"></span></div><div class="col-md-2 mb-1"><button type="button" class="btn btn-success  addshipbutton addshipval addship-'+newid+' addship-div"  id="'+newid+'" value="'+newid+'" style="margin-top:30px">Add</button> <button type="button" class="btn btn-danger  removeshipbutton addremoveshipval addremoveship-'+newid+' addship-div"  id="'+newid+'" value="'+newid+'" style="margin-top:30px">Remove</button></div></div>');
        }
    });
    $(document).on('click','.removeshipbutton',function()
    {
        var id=this.id;
        var myship=$('.myship').length;
        var newid=parseInt(id) - 1;
        $('.ourship-'+id).remove();
        if(myship == 1)
        {
            $('.addship-'+newid).show();
        }
        else
        {
            $('.addship-'+newid).show();
            $('.addremoveship-'+newid).show();
        }
    });
    $(document).on('click','.adddisbutton',function()
    {
        var lastid1=$(this).val();
        var lastid=this.id;
        error=0;
        if($('#discount_qty_'+lastid1).val()=="")
        {
            error++;
            $('#discount_qty_'+lastid1+'_err').text("Please fill this field");
            $('#discount_qty_'+lastid1+'__err').show();
        }
        else
        {
            $('#discount_qty_'+lastid1+'__err').hide();
            $('#discount_qty_'+lastid1+'__err').text('');
        }
        if($('#discount_'+lastid1).val()=="" || $('#discount_'+lastid1).val()=="0"  )
        {
            error++;
            $('#discount_'+lastid1+'__err').text("Please fill this field");
            $('#discount_'+lastid1+'__err').show();
        }
        else
        {
            $('#discount_'+lastid1+'__err').hide();
            $('#discount_'+lastid1+'__err').text('');
        }
        if(error==0)
        {
            var lastprice=$('#product_main_discount_'+lastid1).val();
            $('.adddis-'+lastid1).hide();
            $('.addremove-'+lastid1).hide();
            var newid=parseInt(lastid1) + 1;
            var newdiscountqty=$('#discount_qty_'+lastid1).val();
            var newdisco=parseInt(newdiscountqty) + 3;
            $('.discountdata').append('<div class="row mydis ourdis-'+newid+'" id="'+newid+'"><div class="col-md-2 mb-1"><label for="product_discount_'+newid+'">Discount*</label><select class="custom-select form-control textfield" name="product_discount[]" id="product_discount_'+newid+'"><option value="flat_discount">Flat</option><option value="per_discount" selected>%age</option></select><span class="text-danger" id="product_discount_'+newid+'_err" style="color:red;"></span></div><div class="col-md-2 mb-1"><label for="discount_qty_'+newid+'">Enter Qty</label><input type="text" class="form-control" name="discount_qty[]" id="discount_qty_'+newid+'" value="'+newdisco+'"></div><div class="col-md-2 mb-1"><label for="discount_'+newid+'">Enter Discount</label><input type="text" class="form-control discount" name="discount[]" id="discount_'+newid+'" value="0"></div><div class="col-md-2 mb-1"><label for="product_main_discount_'+newid+'">Discount Price</label><input type="text" class="form-control" name="product_main_discount[]" id="product_main_discount_'+newid+'" value="0" readonly></div><div class="col-md-2"><button type="button" class="btn btn-success  adddisbutton adddisval adddis-'+newid+' adddis-div"  id="'+newid+'" value="1" style="margin-top:30px">Add</button><button type="button" class="btn btn-danger  removedisbutton addremoveval addremove-'+newid+' adddis-div"  id="'+newid+'" value="'+newid+'" style="margin-top:30px">Remove</button></div><input type="hidden" id="maindis_'+newid+'" name="maindis" class="maindis" value="'+lastprice+'">');
        }
    });
    $(document).on('click','.removedisbutton',function()
    {
        var id=this.id;
        var mydis=$('.mydis').length;
        var newid=parseInt(id) - 1;
        $('.ourdis-'+id).remove();
        if(mydis == 1)
        {
            $('.adddis-'+newid).show();
        }
        else
        {
            $('.adddis-'+newid).show();
            $('.addremove-'+newid).show();
        }
    });
    $(document).on('keyup','.discount',function()
    {
        var dislength=$('.discount').length;
        var discountprice=$('#discount_'+dislength).val();
        var mainprice= $('#maindis_'+dislength).val();
        if($('#regular_price').val()=="")
        {
            $('#discount_'+dislength+'_error').text("Please Enter Regular Price");
            $('#regular_price').focus();
        }
        else
        {
            if(mainprice=="")
            {
                mainprice=0;
            }
            if(discountprice=="")
            {
                discountprice=0;
            }
            var discountafter= (parseFloat(mainprice) * parseFloat(discountprice)) / 100;
            var ckprice=parseFloat(mainprice) - parseFloat(discountafter);
            $('#product_main_discount_'+dislength).val(ckprice.toFixed(2));
            $('#chkdiscountprice_'+dislength).text("Discount Price : $"+ckprice.toFixed(2));
        }
    });
    $(document).on('blur','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {

            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('keyup change','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('click','#publish',function()
    {
        $('.text-danger').hide();
        $('.text-danger').val('');
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                $("#"+this.id+"_err").text("Please fill this field");
                $("#"+this.id+"_err").show();
                isValid = false;        
            }
        });
        if(isValid)
        {
            var formdata=$(".product_form").serialize();
            $.ajax(
            {
                url: "{{ url('add_product') }}",
                type: 'POST',
                data: formdata,
                success: function(data) 
                {
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Added",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        $(".product_form")[0].reset();
                        $('#product_title').focus();
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });
</script>
@include('vendor.include.footer')