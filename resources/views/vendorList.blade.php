@include('include.header')
@include('include.sidebar')
         
   <div class="page  has-sidebar-left height-full">
    <header class="blue accent-3 relative">
        <div class="container-fluid text-white">
            <div class="row p-t-b-10 ">
                <div class="col">
                    <h4>
                        <i class="icon-database"></i>
                        Users
                    </h4>
                </div>
            </div>
            <div class="row justify-content-between">
                <ul class="nav nav-material nav-material-white responsive-tab" id="v-pills-tab" role="tablist">
                    <li>
                        <a class="nav-link active" id="v-pills-all-tab" data-toggle="pill" href="#v-pills-all" role="tab" aria-controls="v-pills-all"><i class="icon icon-home2"></i>All Users</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-buyers-tab" data-toggle="pill" href="#v-pills-buyers" role="tab" aria-controls="v-pills-buyers"><i class="icon icon-face"></i> Buyers</a>
                    </li>
                    <li>
                        <a class="nav-link" id="v-pills-sellers-tab" data-toggle="pill" href="#v-pills-sellers" role="tab" aria-controls="v-pills-sellers"><i class="icon  icon-local_shipping"></i> Sellers</a>
                    </li>
                    <li class="float-right">
                        <a class="nav-link" href="panel-page-users-create.html"><i class="icon icon-plus-circle"></i> Add New User</a>
                    </li>
                </ul>
            </div>
        </div>
    </header>
    <div class="container-fluid animatedParent animateOnce">
        <div class="tab-content my-3" id="v-pills-tabContent">
            <div class="tab-pane animated fadeInUpShort show active go" id="v-pills-all" role="tabpanel" aria-labelledby="v-pills-all-tab">
                <div class="row my-3">
                    <div class="col-md-12">
                        <div class="card r-0 shadow">
                            <div class="table-responsive">
                                <form >
                                  @csrf
                                      <div class="container-fluid animatedParent animateOnce my-3">
                                      <div class="animated fadeInUpShort go">
                                      <div class="row">
                                          <div class="col-md-3">
                                           <div class="form-group">
                                            <input type="text" name="serach" id="serach"   placeholder="Search For..." class="form-control" />
                                           </div>
                                          </div>
                                          <div class="col-md-12">
                                              <div class="card no-b shadow">
                                                  <div class="card-body p-0 pagination a">

                                                          @include('pagination_data')
                                                   </div>
                                              </div>

                                              <input type="hidden" name="hidden_page" id="hidden_page" value="1" />         
                                          </div>
                                      </div>
                                    </div>
                                </div>
                             </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
         </div> 
     </div>            

<script>

$(document).ready(function(){
 
 function fetch_data(page,query)
 { 
  $.ajax({
   url:"vendorList/fetch_data?page="+page+"&query="+query,
   success:function(data)
   {    
    $('table').html('');
    $('table').html(data);
   }
  })
 }

 $(document).on('keyup', '#serach', function(){
  var query = $('#serach').val();
  var page = $('#hidden_page').val();
  fetch_data(page,query);
 });

 $(document).on('click', '.pagination a', function(event){
  event.preventDefault();
  var page = $(this).attr('href').split('page=')[1];
  $('#hidden_page').val(page);
  var query = $('#serach').val();

  $('li').removeClass('active');
   $(this).parent().addClass('active');
  fetch_data(page,query);
 });


});
</script>
@include('include.footer')


