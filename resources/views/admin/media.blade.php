
 <!--   media link -->
       <link href="{{asset('public/media/dist/mediamanager.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/media/dist/dropzone.min.css')}}" rel="stylesheet" type="text/css" />
       <link href="{{asset('public/media/src/summernote.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{asset('public/media/src/zenburn.css')}}" rel="stylesheet" type="text/css" />
       <script src="{{asset('public/media/src/summernote.js')}}" type="text/javascript"></script>
            <script src="{{asset('public/media/src/highlight.pack.js')}}" type="text/javascript"></script>
      <script src="{{asset('public/media/dist/dropzone.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('public/media/dist/mediamanager.min.js')}}" type="text/javascript"></script>
      <!-- end media -->
    <body>
    <div class="container">
    	{{ csrf_field() }}
        <!-- <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-12">
                <div class="jumbotron text-center">
                    <h1>Media Manager</h1>
                    <h3>A Vanilla Javascript Media Manager</h3>
                </div>
            </div>
        </div> -->
      <!--   <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-12 text-center">
                <h2>Media Manager is built by mimicking Wordpress Media Manager. <br>
                    As iam no good with words, lets take a look what this plugin does.
                </h2>
            </div>
        </div> -->
     <!--    <div class="row" style="margin-bottom: 50px;display: none" >
            <div class="col-md-10 col-md-push-1">
                
                <div id="summernoteMedia" class="btn btn-primary" style="margin-bottom: 10px;">Media Manager</div>
              
                <div style="margin-top: 20px;">
           

                </div>
            </div>
        </div>
        <div class="row" style="margin-bottom: 50px;display: none">
            <div class="col-md-10 col-md-push-1">
               
                <div id="ckeditorMedia" class="btn btn-primary" style="margin-bottom: 10px;">Media Manager</div>
                <div id="editor">
                   
                </div>
                <div style="margin-top: 20px;">
               
                </div>
            </div>
        </div> -->
        
        <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-10 col-md-push-1">
               
                <div id="allOptionsButton" class="btn btn-primary allOptionsButton">Media Manager</div>
                <div style="margin-top: 20px; margin-bottom: 20px;">
                  
                </div>
              
            </div>
        </div>
        
    </div>

    <script>
        $(document).ready(function() {
        	



            var mediamanager = new Mediamanager({
                loadItemsUrl: '{{route("loadfile_json")}}',
            });

          

            var mediamanager2 = new Mediamanager({
                buttonText: 'Insert to Post',
                deleteButtonText: 'Delete',
                deleteConfirmationText: 'Are you sure?',
                deleteItemUrl: '/',
                loadItemsUrl: '{{url("admin/loadfile")}}',
                loadNextItemsUrl: '/hirola/loadnextfiles.json',
                loadNextItemDelay: 3000,
                uploadUrl: '{{route("media_upload")}}',
                insertType: 'object',
                insert: function (data) {
                    console.log(data);
                }
            });

            $('#allOptionsButton').click(function () {
               mediamanager2.open();
            });
        });
    </script>
  
</body>
