@include('vendor.include.header')
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    @include('vendor.include.menu')
    <!--Sidebar End-->
    <div class="has-sidebar-left">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                    <div class="search-bar">
                        <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                               placeholder="start typing...">
                    </div>
                    <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                       aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
                </div>
            </div>
        </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <li>
                            <a class="nav-link " data-toggle="collapse" data-target="#navbarToggleExternalContent"
                               aria-controls="navbarToggleExternalContent"
                               aria-expanded="false" aria-label="Toggle navigation">
                                <i class=" icon-search3 "></i>
                            </a>
                        </li>
                        <!-- User Account-->
                        <li class="dropdown custom-dropdown user user-menu ">
                            <a href="#" class="nav-link" data-toggle="dropdown">
                                <img src="{{ asset('vendor-assets/img/dummy/u8.png') }}" class="user-image" alt="User Image">
                                <i class="icon-more_vert "></i>
                            </a>
                            <div class="dropdown-menu p-4 dropdown-menu-right">
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Apps</div>
                                        </a>
                                    </div>
                                    <div class="col"><a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a></div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-star light-green lighten-1 avatar  r-5"></i>
                                            <div class="pt-1">Favourites</div>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-save2 orange accent-1 avatar  r-5"></i>
                                            <div class="pt-1">Saved</div>
                                        </a>
                                    </div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting grey darken-3 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                                <hr>
                                <div class="row box justify-content-between my-4">
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Apps</div>
                                        </a>
                                    </div>
                                    <div class="col"><a href="#">
                                        <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                                        <div class="pt-1">Profile</div>
                                    </a></div>
                                    <div class="col">
                                        <a href="#">
                                            <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                            <div class="pt-1">Settings</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            {{$data['title']}}
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link" href="#"><i class="icon icon-list"></i>All Shipping Services</a>
                        </li>
                        <li>
                            <a class="nav-link active" href="#"><i
                                    class="icon icon-plus-circle"></i> Add New Shipping Service</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="container-fluid animatedParent animateOnce my-3">
            <div class="animated fadeInUpShort">
                <form id="needs-validation" class="ship_form" novalidate>
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="ship_title">Title*</label>
                                    <input type="text" class="form-control textfield" id="ship_title" name="ship_title" placeholder="Title" required autofocus>
                                    <span class="text-danger" id="ship_title_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="hue-demo">Title Color*</label>
                                    <div class="card-body b-b">
                            <div class="card-title">Setting the initial color</div>
                            <div class="color-picker input-group">
                                <input type="text" value="#00AABB" class="form-control"/>
                                <span class="input-group-append">
                        <span class="input-group-text add-on white">
                            <i class="circle"></i>
                      </span>
                    </span>
                            </div>
                            <div class="color-picker input-group my-3">
                                <input type="text" value="#e443b1" class="form-control"/>
                                <span class="input-group-append">
                        <span class="input-group-text add-on">
                            <i></i>
                      </span>
                    </span>
                            </div>
                        </div>
                                    <span class="text-danger" id="hue-demo_err" style="color:red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Publish Box</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="button" id="publish">Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="{{ asset('vendor-assets/js/app.js') }}"></script>
<script>
    $(document).on('blur','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {

            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('keyup change','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {
            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }); 
    $(document).on('click','.adddisbutton',function()
    {
        var lastid1=$(this).val();
        var lastid=this.id;
        if($('#weight_label_name_'+lastid1).val().trim()=="")
        {
            $('#weight_label_name_'+lastid1+'_err').text("Please fill this field");
            $('#weight_label_name_'+lastid1+'_err').show();
        }
        else
        {
            $('#weight_label_name_'+lastid1+'_err').text("");
            $('#weight_label_name_'+lastid1+'_err').hide();
            var newid=parseInt(lastid1) + 1;
            $('.adddis-'+lastid1).hide();
            $('.addremove-'+lastid1).hide();
            $('<div class="row weightdata"><div class="col-md-8 mb-6"><label for="tax_rate">Weight Label Name*</label><input type="text" class="form-control textfield" id="weight_label_name_'+newid+'" name="weight_label_name[]" placeholder="Weight Label Name" required><span class="text-danger" id="weight_label_name_'+newid+'_err" style="color:red;"></span><div><div class="col-md-4 mb-6"><button type="button" class="btn btn-success  adddisbutton adddisval adddis-'+newid+' adddis-div"  id="'+newid+'" value="'+newid+'" style="margin-top:30px">Add More</button><button type="button" class="btn btn-danger  removedisbutton addremoveval addremove-'+newid+' adddis-div"  id="'+newid+'" value="'+newid+'" style="margin-top:30px">Remove</button></div></div>').insertAfter('.weightdata:last');
        }
    });
    $(document).on('click','#publish',function()
    {
        $('.text-danger').hide();
        $('.text-danger').val('');
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                $("#"+this.id+"_err").text("Please fill this field");
                $("#"+this.id+"_err").show();
                isValid = false;        
            }
        });
        if(isValid)
        {
            var formdata=$(".weight_form").serialize();
            $.ajax(
            {
                url: "{{ url('add_weight') }}",
                type: 'POST',
                data: formdata,
                success: function(data) 
                {
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Added",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        $(".weight_form")[0].reset();
                        $('#weight_name').focus();
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });
</script>
@include('vendor.include.footer')