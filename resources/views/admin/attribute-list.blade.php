@include('admin.include.header')
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    @include('admin.include.menu')
    <!--Sidebar End-->
    <div class="has-sidebar-left">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <div class="bg-dark pt-2 pb-2 pl-4 pr-2">
                    <div class="search-bar">
                        <input class="transparent s-24 text-white b-0 font-weight-lighter w-128 height-50" type="text"
                               placeholder="start typing...">
                    </div>
                    <a href="#" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-expanded="false"
                       aria-label="Toggle navigation" class="paper-nav-toggle paper-nav-white active "><i></i></a>
                </div>
            </div>
        </div>
        <div class="sticky">
            <div class="navbar navbar-expand navbar-dark d-flex justify-content-between bd-navbar blue accent-3">
                <div class="relative">
                    <a href="#" data-toggle="push-menu" class="paper-nav-toggle pp-nav-toggle">
                        <i></i>
                    </a>
                </div>
                <!--Top Menu Start -->
    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li>
                <a class="nav-link " data-toggle="collapse" data-target="#navbarToggleExternalContent"
                   aria-controls="navbarToggleExternalContent"
                   aria-expanded="false" aria-label="Toggle navigation">
                    <i class=" icon-search3 "></i>
                </a>
            </li>
            <!-- Right Sidebar Toggle Button -->
            <!-- User Account-->
            <li class="dropdown custom-dropdown user user-menu ">
                <a href="#" class="nav-link" data-toggle="dropdown">
                    <img src="{{ asset('vendor-assets/img/dummy/u8.png') }}" class="user-image" alt="User Image">
                    <i class="icon-more_vert "></i>
                </a>
                <div class="dropdown-menu p-4 dropdown-menu-right">
                    <div class="row box justify-content-between my-4">
                        <div class="col">
                            <a href="#">
                                <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                <div class="pt-1">Apps</div>
                            </a>
                        </div>
                        <div class="col"><a href="#">
                            <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                            <div class="pt-1">Profile</div>
                        </a></div>
                        <div class="col">
                            <a href="#">
                                <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                <div class="pt-1">Settings</div>
                            </a>
                        </div>
                    </div>
                    <div class="row box justify-content-between my-4">
                        <div class="col">
                            <a href="#">
                                <i class="icon-star light-green lighten-1 avatar  r-5"></i>
                                <div class="pt-1">Favourites</div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="#">
                                <i class="icon-save2 orange accent-1 avatar  r-5"></i>
                                <div class="pt-1">Saved</div>
                            </a>
                        </div>
                        <div class="col">
                            <a href="#">
                                <i class="icon-perm_data_setting grey darken-3 avatar  r-5"></i>
                                <div class="pt-1">Settings</div>
                            </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row box justify-content-between my-4">
                        <div class="col">
                            <a href="#">
                                <i class="icon-apps purple lighten-2 avatar  r-5"></i>
                                <div class="pt-1">Apps</div>
                            </a>
                        </div>
                        <div class="col"><a href="#">
                            <i class="icon-beach_access pink lighten-1 avatar  r-5"></i>
                            <div class="pt-1">Profile</div>
                        </a></div>
                        <div class="col">
                            <a href="#">
                                <i class="icon-perm_data_setting indigo lighten-2 avatar  r-5"></i>
                                <div class="pt-1">Settings</div>
                            </a>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
            </div>
        </div>
    </div>
    <div class="page has-sidebar-left">
        <header class="blue accent-3 relative">
            <div class="container-fluid text-white">
                <div class="row p-t-b-10 ">
                    <div class="col">
                        <h4>
                            <i class="icon-package"></i>
                            {{$data['title']}}
                        </h4>
                    </div>
                </div>
                <div class="row">
                    <ul class="nav responsive-tab nav-material nav-material-white">
                        <li>
                            <a class="nav-link active" href="{{url('admin/attribute-list')}}"><i
                                    class="icon icon-list"></i> All Attributes</a>
                        </li>
                        <li>
                            <a class="nav-link" href="{{url('admin/attribute')}}"><i
                                    class="icon icon-plus-circle"></i> Add New Attribute</a>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
        <div class="content-wrapper animatedParent animateOnce">
            <div class="container">
                <section class="paper-card">
                    <div class="row">
                        <div class="col-md-3">  <label for="search">Search</label>
                            <input id="search" type="text" placeholder="Search" class="form-control" name="search" autocomplete="off">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="box result_append">
                                <div class="box-body" id="tag_container">
                                    <table class="table table-bordered">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">#</th>
                                            <th>Attribute</th>
                                            <th>Slug</th>
                                            <th>Seo Key</th>
                                            <th>Meta Description</th>
                                        </tr>
                                        @if(count($attribute)>0)
                                            <?php $i=1; ?>
                                            @foreach($attribute as $attrs)
                                                <tr>
                                                    <td>{{$i}}</td>
                                                    <td id="attribute_name{{$attrs->attribute_id}}">{{$attrs->attribute_name}}</td>
                                                    <td id="attribute_type{{$attrs->attribute_id}}">{{$attrs->attribute_type}}</td>
                                                    <td id="attribute_selection{{$attrs->attribute_id}}">{{$attrs->attribute_selection}}</td>
                                                    <td><span class="edit" id="{{$attrs->attribute_id}}">Edit</span></td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                        @else
                                            <tr>
                                                <th colspan="5">No Data Found</th>
                                            </tr>
                                        @endif
                                        </tbody>
                                    </table>
                                    {{$attribute->render()}}
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Category</h4>
      </div>
      <div class="modal-body">
        <form id="needs-validation" class="attribute_form" novalidate enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="col-md-8 ">
                            <div class="row">
                                <div class="col-md-6 mb-3">
                                    <label for="attribute_selection">Attribute/Types*</label>
                                    <input type="hidden" id="attribute_id" name="attribute_id">
                                    <select id="attribute_selection" name="attribute_selection" class="custom-select form-control textfield" required autofocus>
                                        <option value="">Select Attribute/Types</option>
                                        <option value="attribute">Attribute</option>
                                        <option value="item_specifics">Types</option>
                                    </select>
                                    <span class="text-danger" id="attribute_selection_err" style="color:red;"></span>
                                </div>
                                <div class="col-md-6 mb-3">
                                    <label for="attribute_type">Type*</label>
                                    <select id="attribute_type" name="attribute_type" class="custom-select form-control textfield" required>
                                        <option value="">Select Type</option>
                                        <option value="select">Select</option>
                                        <option value="image">Image</option>
                                        <option value="label">Label</option>
                                        <option value="checkbox">Checkbox</option>
                                    </select>
                                    <span class="text-danger" id="attribute_type_err" style="color:red;"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 mb-6">
                                    <label for="attribute_name">Enter Name*</label>
                                    <input type="text" class="form-control textfield" id="attribute_name" name="attribute_name" placeholder="Enter Name" required>
                                    <span class="text-danger" id="attribute_name_err" style="color:red;"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="card mt-4">
                                <h6 class="card-header white">Publish Box</h6>
                                <div class="card-footer bg-transparent">
                                    <button class="btn btn-primary" type="button" id="publish">Publish</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@include('admin.include.footer')
<script>
    $(window).on('hashchange', function() 
    {
        if (window.location.hash) 
        {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) 
            {
                return false;
            }
            else
            {
                getData(page);
            }
        }
    });
    $(document).ready(function()
    {
        $(document).on('click', '.pagination a',function(event)
        {
            event.preventDefault();
            var page=$(this).attr('href').split('page=')[1];
            getData(page);
        });
    });
    function getData(page)
    {
        var search=$('#search').val();
        $.ajax(
        {
            url: '?page=' + page+'&search='+search,
            type: "get",
            datatype: "html"
        }).done(function(data)
        {
            var data1=jQuery(data).find('.result_append').html();
            $(".result_append").empty().html(data1);
            location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError)
        {
          alert('No response from server');
      });
    }
    $(document).on('keyup','#search',function(e)
    {
        do_stuff();
    });
    function do_stuff()
    {
        var search=$('#search').val();
        $.ajax(
        {
            url: '{{route("attribute_list")}}',
            data: {
            'search':search,
            '_token':'{{csrf_token()}}'
            },
            type: 'POST',
            success: function (data) 
            {
            var data1=jQuery(data).find('.result_append').html();
            $(".result_append").empty().html(data1);
            }
        });
    }
    $(document).on('click','.edit',function()
    {
        var id=this.id;
        $.ajax(
        {
            url: '{{route("fetch_attribute_data")}}',
            data: {
            'attribute_id':this.id,
            '_token':'{{csrf_token()}}'
            },
            type: 'POST',
            dataType: "json",
            success: function (data) 
            {
                $('#attribute_id').val(data.attribute_id);
                $('#attribute_type').val(data.attribute_type);
                $('#attribute_selection').val(data.attribute_selection);
                $('#attribute_name').val(data.attribute_name);
                $('#editModal').modal('show');
            }
        });
    });
    $(document).on('blur','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {

            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    });
    $(document).on('keyup change','.textfield',function()
    {
        if($(this).val().trim()=="")
        {
            $("#"+this.id+"_err").text("Please fill this field");
            $("#"+this.id+"_err").show();
        }
        else
        {

            $("#"+this.id+"_err").text("");
            $("#"+this.id+"_err").hide();
        }
    }); 
    $(document).on('click','#publish',function()
    {
        $('.text-danger').hide();
        $('.text-danger').val('');
        var isValid = true;
        $('.textfield').each(function()
        {
            if($(this).val().trim()=="")
            {
                $("#"+this.id+"_err").text("Please fill this field");
                $("#"+this.id+"_err").show();
                isValid = false;  
            }
        });
        if(isValid)
        {
            var formData=$(".attribute_form").serialize();
            $.ajax(
            {
                url: "{{ route('edit_attribute') }}",
                type: 'POST',
                data: formData,
                success: function(data) 
                {
                    if(data.status=='success')
                    {
                        Swal.fire({
                        title: "Successfully Updated",
                        text: data.msg,
                        type: "success",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false
                        });
                        var id=$('#attribute_id').val().trim();
                        $('#attribute_type'+id).text($('#attribute_type').val().trim());
                        $('#attribute_selection'+id).text($('#attribute_selection').val().trim());
                        $('#attribute_name'+id).text($('#attribute_name').val().trim());
                        $(".attribute_form")[0].reset();
                        $('#editModal').modal('hide');
                    }
                    else
                    {
                        Swal.fire({
                        title: "Notice",
                        text: data.msg,
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#DD6B55",
                        confirmButtonText: "Ok",
                        cancelButtonText:false,
                        closeOnConfirm: false,
                        closeOnCancel: false,
                        dangerMode: true,
                        });
                    }
                },
            });
        }
        else
        {
            Swal.fire({
            title: "Notice",
            text: 'Please fill all required fields',
            type: "warning",
            showCancelButton: false,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Ok",
            cancelButtonText:false,
            closeOnConfirm: false,
            closeOnCancel: false,
            dangerMode: true,
            });
        }
    });
</script>