 @extends('layouts.app')
 
    @section('content')
     <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <!-- Main-body start -->
                            <div class="main-body">
                                <div class="page-wrapper">
                                    <!-- Page-header start -->
                                    <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Add Category</h4>
                                                       <!--  <span>Lorem ipsum dolor sit <code>amet</code>, consectetur
                                                            adipisicing elit</span> -->
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item"  style="float: left;">
                                                            <a href="{{url('/admin/')}}"> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Master</a>
                                                        </li>
                                                        <li class="breadcrumb-item"  style="float: left;"><a href="#!">Add Category</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <!-- Page body start -->
                                    <div class="page-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <!-- Basic Form Inputs card start -->
                                                <div class="card">
                                                    
                                                    <div class="card-block">
                                                        <h4 class="sub-title">Add Category</h4>
                                                        <form>
                                                            <div class="form-group row">
                                                                <label class="col-sm-2 col-form-label">Category Name<sup>*</sup></label>
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control form-control-round form-control-uppercase textfield" id="category" name="category" placeholder="Category Name" required autofocus>
                                                                     <span class="text-danger col-form-label" id="category_err" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-sm-2 col-form-label">SEO Keywords</label>
                                                                <div class="col-sm-10">
                                                                    <input type="text"  class="form-control form-control-round" id="seokey" name="seokey" placeholder="SEO Keywords" required>
                                                                </div>
                                                            </div>
                                                             <div class="form-group row">
                                                                <label
                                                                    class="col-sm-2 col-form-label">Meta Description</label>
                                                                <div class="col-sm-10">
                                                                    <textarea class="form-control" id="meta_description" name="meta_description"
							                                              placeholder="Write Something..."  required></textarea>
							                                    <span class="text-danger" id="meta_description_err" style="color:red;"></span>
                                                                </div>
                                                            </div>
                                                           
                                                        </form>
                                                        
                                                    </div>
                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page body end -->
                                </div>
                            </div>
                            <!-- Main-body end -->
                            <div id="styleSelector">

                            </div>
                        </div>
                    </div>
    @endsection