<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCatalogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catalogs', function (Blueprint $table) {
            $table->increments('cat_id');
            $table->text('cat_name')->nullable();
            $table->text('cat_slug')->nullable();
            $table->text('seokey')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('cat_image')->nullable();
            $table->integer('cat_order')->default('0');
            $table->integer('cat_status')->default('0');
            $table->string('cat_date',10)->nullable();
            $table->string('cat_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catalogs');
    }
}
