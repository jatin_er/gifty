<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attributes', function (Blueprint $table) {
            $table->increments('attribute_id');
            $table->text('attribute_name')->nullable();
            $table->text('attribute_type')->nullable();
            $table->text('attribute_selection')->nullable();
            $table->integer('attribute_order')->default('0');
            $table->integer('attribute_status')->default('0');
            $table->string('attribute_date',10)->nullable();
            $table->string('attribute_time',10)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attributes');
    }
}
